import { Component, Output, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { EventEmitter } from 'events';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  form:FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });

  constructor(private router:Router){}

  Login(){
    alert("Usuario Registrado");
    this.router.navigate(["Login"]);
  }

  Listar(){
    this.router.navigate(["listar"]);
  }

  Nuevo(){
    this.router.navigate(["add"]);
  }

  login(){
    if(this.form.valid){
      this.submitEM.emit(this.form.value);
      this.router.navigate(["listar"]);
    }
  }

  @Input() error: string | null;

  @Output() submitEM = new EventEmitter();
}
