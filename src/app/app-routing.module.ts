import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarComponent } from './Persona/listar/listar.component';
import { AddComponent } from './Persona/add/add.component';
import { EditComponent } from './Persona/edit/edit.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  {path:"Login", component:LoginComponent},
  {path:"", component:LoginComponent,
    children:[{
      component:LoginComponent,
      path: ""
    }]},
  {path:"listar", component:ListarComponent},
  {path:"add", component:AddComponent},
  {path:"edit", component:EditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
