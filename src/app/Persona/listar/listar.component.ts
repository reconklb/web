import { Component, OnInit, PipeTransform } from '@angular/core';
import { ServiceService } from 'src/app/Service/service.service';
import { Router } from '@angular/router';
import { Persona } from 'src/app/Modelo/persona';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {MDCDataTable} from '@material/data-table';
import { stringify } from 'querystring';
//const dataTable = new MDCDataTable(document.querySelector('.mdc-data-table'));


@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {
  closeResult: string;

  per={
    id: null,
    email:null,
    password:null,
    createdAt: null,
    updatedAt: null,
    nombre: null,
    apellido: null
  }

  personas:Persona[];
  constructor(private service:ServiceService, private router:Router,private modalService: NgbModal) { }

  ngOnInit() {
    this.service.getPersonas()
    .subscribe(data=>{
      this.personas=data;
    })
  }




  open(content) {
    this.modalService.open(content, {backdropClass: 'light-blue-backdrop',size: 'lg', ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  edit(content1, id: string,per) {
    this.per.id = per.id;
    this.per.nombre = per.nombre;
    this.per.email = per.email;
    this.per.password = per.password;
    this.per.createdAt = per.createdAt;
    this.per.updatedAt = per.updatedAt;
    this.per.apellido =per.apellido;

    //this.service.getPersonaById(id).subscribe(data=>{this.personas=data;})
    
    this.modalService.open(content1, {backdropClass: 'light-blue-backdrop',size: 'lg', ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  Guardar(persona:Persona){
    this.service.createPersona(persona)
    .subscribe(data=>{
      alert("Se agrego con exito...!!!");
      this.router.navigate(["listar"]);
    })
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  
}
