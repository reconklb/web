import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Persona } from '../Modelo/persona';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {


  constructor(private http:HttpClient) { }

  //url="http://localhost:8080/apiREST/api";
  url="http://localhost:8080/api/users/";
  

  getPersonas(){
    return this.http.get<Persona[]>(this.url);
  }
  
  getPersonaById(id: string){
    const urlEditar="http://localhost:8080/api/users/" + id;
    console.log(urlEditar);
    return this.http.get<Persona[]>(urlEditar).pipe(
      tap(
        (error) => ('Error el buscar el id')
      )
    );
  }

  createPersona(persona:Persona){
    return this.http.post<Persona>(this.url,persona);
  }
}
