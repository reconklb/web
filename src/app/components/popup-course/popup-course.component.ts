import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {LogService} from '../../Service/log/log.service';

@Component({
  selector: 'app-popup-course',
  templateUrl: './popup-course.component.html',
  styleUrls: ['./popup-course.component.css']
})
export class PopupCourseComponent implements OnInit {


  private logService: LogService;
  private LOG_TAG: 'POPUP_COURSE_COMPONENT: ';

  constructor(private dialogRef: MatDialogRef<PopupCourseComponent>) {
    this.logService = new LogService(this.LOG_TAG);
  }

  ngOnInit() {
  }

  closeWithoutSave() {
    this.logService.print('Closed without save', LogService.DEFAULT_MSG);
    this.dialogRef.close();
  }

}
